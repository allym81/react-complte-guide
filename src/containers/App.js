import React, { Component } from 'react';
import classes from './App.css';
import Person from '../components/Persons/Person/Person';
 
class App extends Component {

  state = {
    persons: [
      { id: 'asfa1', name: 'Max', age: 28 },
      { id: 'vasdf1', name: 'Manu', age: 29 },
      { id: 'asdf11', name: 'Stephanie', age: 26 }
    ],
    otherState: 'some other value',
    showPersons: false
  }
  switchNamekHandler = (newName) => {
    console.log('was clicked');
    this.setState({
      persons:[
      {name :newName, age: 28},
      {name : 'Manu', age : 29},
      {name : 'Steph', age: 31}
    ],
    showPersons : false
  });
  }

  togglePersonHandler = () =>{
    const doesShow= this.state.showPersons;
      this.setState({showPersons :!doesShow});
  }

  nameChangedHandler=(event, id)=>{
    const personIndex= this.state.persons.findIndex(p=>{
      return p.id===id
    });

    const person = {...this.state.persons[personIndex]};
    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;
    console.log(event.target.value);
    this.setState({persons:persons});
  }

  deletePersonHandler =(personIndex)=>{
    const persons = [...this.state.persons];
    persons.splice(personIndex,1);
    this.setState({persons: persons});
  }

  render() {
   
    let persons = null;
    let btnClass=null;
    if(this.state.showPersons){
      persons = (
        <div>
          {this.state.persons.map((person, index)=>{
            return <Person key={person.id}
                name={person.name} 
                age={person.age} 
                changed={(event)=>this.nameChangedHandler(event, person.id)}
                click={()=>this.deletePersonHandler(index)}
                />
          })
          }
      </div> 
      );
      btnClass = classes.Red;
    }

    const assignedClasses =[];
    if(this.state.persons.length>=2){
      assignedClasses.push(classes.Red);
     
    }
    if(this.state.persons.length>=1){
      assignedClasses.push(classes.bold);
    }
   return(
    <div className={classes.App}>
      <h1>My React App</h1>
      <button className={btnClass} onClick={this.togglePersonHandler}>Switch Name</button>
      {persons}
   </div>
   );
     
   // return React.createElement('div', {className:"App"},React.createElement('h1',null,'Working react with child') );
  }
}

export default App;
